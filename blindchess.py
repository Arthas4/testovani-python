

class Position():
    x = None
    y = None

    def __init__(self, x, y):
        if not ('a' <= x <= 'h'):
            raise ValueError("X needs to be str of a-h. Paased: {}".format(x))
        if not (1 <= y <= 8):
            raise ValueError("Y needs to be int of 1-8. Paased: {}".format(y))
        self.x = x
        self.y = y

    def __str__(self):
        return "Position({}, {})".format(self.x, self.y)
    def __repr__(self):
        return "({}, {})".format(self.x, self.y)
    def __eq__(self, other):
        if not isinstance(other, Position):
            return NotImplemented
        else:
            return self.x == other.x and self.y == other.y

    def x_up(self, size=1):
        return Position(chr(ord(self.x) + size), self.y)
    def y_up (self, size=1):
        return Position(self.x, self.y + size)
    def x_y_change(self, size, size1):
        return Position(chr(ord(self.x) + size), self.y + size1)

    def chess_move(self, first, second, kladne_zaporne):
        for i in range(1,(abs(a - b)+1)):
            a = self.position.x_up(size=(i)*(kladne_zaporne))
            b = c.get_figure_on_position_color(self.color, a)
            if b is None and i == abs(prvni - druha):
                print("tah byl uspesny")
                self.position.x = to_position.x
                self.position.y = to_position.y
                break
            elif b is None:
                pass



            elif b[0] == "ally":
                  raise ValueError("nejde stoji zde " + b[1] + " figura")

            elif b[0] == "enemy":
                if i == abs(prvni - druha):
                        c.del_figure(b[2])
                        print("tah byl uspesny vzali jste figuru")
                        self.position.x = to_position.x
                        self.position.y = to_position.y
                else:
                    raise ValueError("nelze")

        #definicni podminky obecma verze pro vsechny figury



class Figure():
    color = None
    position = None

    def __init__(self, color, position):
        if not isinstance(position, Position):
            raise ValueError("position needs to be instanco of Position. Passed: {}".format(position))
        self.color = color
        self.position = position

    def move(self, to_position):
        raise NotImplemented()

class Chessboard():
    def __init__(self):

         self.figures = [

         Pawn('white', Position('a', 2)),
         Pawn('white', Position('b', 2)),
         Pawn('white', Position('c', 2)),
         Pawn('white', Position('d', 2)),
         Pawn('white', Position('e', 2)),
         Pawn('white', Position('f', 2)),
         Pawn('white', Position('g', 2)),
         Pawn('white', Position('h', 2)),
         Rook('white', Position('a',1)),
         Rook('white', Position('h', 1)),
         Knight('white', Position('b',1)),
         Knight('white', Position('g',1)),
         Bishop('white', Position('c',1)),
         Bishop('white', Position('f',1)),
         Queen('white', Position('d',1)),


         Pawn('black', Position('a', 7)),
         Pawn('black', Position('b', 7)),
         Pawn('black', Position('c', 7)),
         Pawn('black', Position('d', 7)),
         Pawn('black', Position('e', 7)),
         Pawn('black', Position('f', 7)),
         Pawn('black', Position('g', 7)),
         Pawn('black', Position('h', 7)),
         Rook('black', Position('a',8)),
         Rook('black', Position('h', 8)),
         Knight('black', Position('b',8)),
         Knight('black', Position('g',8)),
         Bishop('black', Position('c',8)),
         Bishop('black', Position('f',8)),
         Queen('black', Position('d',8)),
         ]

    def get_figure_on_position_color(self, color, position):

         for figure in self.figures:
            if position.x == figure.position.x and position.y == figure.position.y and color == figure.color:
                return "ally", figure.color, figure

            if position.x == figure.position.x and position.y == figure.position.y and color != figure.color:
                return "enemy", figure.color, figure


            else:
                pass


    def move_with_figure(self, position):
        for figure in self.figures:
            if position == figure.position:
                return figure
    def del_figure(self, figure):
        del self.figures[self.figures.index(figure)]

class Pawn(Figure):

    def move(self, to_position):
        b = c.get_figure_on_position_color(self.color, to_position)
        if (self.position.x == to_position.x) and (self.position.y == to_position.y - 1):

            if b is None:
                print("tah byl uspesny")
                self.position.x = to_position.x
                self.position.y = to_position.y


            elif b[0] == "ally":
                  raise ValueError("nejde stoji zde " + b[1] + " figura")
            elif b[0] == "enemy":
                  raise ValueError("nejde stoji zde "  + b[1] + " figura")

        elif ((ord(self.position.x) == ord(to_position.x) - 1)  and (self.position.y == to_position.y - 1)) or ((ord(self.position.x) == ord(to_position.x) + 1)  and (self.position.y == to_position.y - 1)):
            if b[0] == "enemy":

                print("tah byl uspesny")

                self.position.x = to_position.x
                self.position.y = to_position.y
            else:
                 raise ValueError("nelze")




        else:
             raise ValueError("position of y needs to be: {}".format(self.position.y + 1))

class Rook(Figure):


    def move(self, to_position):
        prvni = ord(self.position.x)
        druha = ord(to_position.x)

        if ((self.position.x != to_position.x) and (self.position.y == to_position.y)):

            if (prvni - druha) < 0:
                for i in range(1,(abs(prvni - druha)+1)):
                    a = self.position.x_up(size=i)
                    b = c.get_figure_on_position_color(self.color, a)
                    if b is None and i == abs(prvni - druha):
                        print("tah byl uspesny")
                        self.position.x = to_position.x
                        self.position.y = to_position.y
                        break
                    elif b is None:
                        pass



                    elif b[0] == "ally":
                          raise ValueError("nejde stoji zde " + b[1] + " figura")

                    elif b[0] == "enemy":
                        if i == abs(prvni - druha):
                                c.del_figure(b[2])
                                print("tah byl uspesny vzali jste figuru")
                                self.position.x = to_position.x
                                self.position.y = to_position.y
                        else:
                            raise ValueError("nelze")

            elif (prvni - druha) > 0:
                for i in range(1,(abs(prvni - druha)+1)):
                    a = self.position.x_up(size=(-i))
                    b = c.get_figure_on_position_color(self.color, a)
                    if b is None and i == abs(prvni - druha):
                        print("tah byl uspesny")
                        self.position.x = to_position.x
                        self.position.y = to_position.y
                        break
                    elif b is None:
                        pass
                    elif b[0] == "ally":
                          raise ValueError("nejde stoji zde " + b[1] + "figura")

                    elif b[0] == "enemy":
                        if i == abs(prvni - druha):
                                c.del_figure(b[2])
                                print("tah byl uspesny vzali jste figuru")
                                self.position.x = to_position.x
                                self.position.y = to_position.y
                        else:
                            raise ValueError("nelze")


        elif (self.position.x == to_position.x) and (self.position.y != to_position.y):
            if self.position.y - to_position.y < 0:
                for i in range(1,(abs(self.position.y - to_position.y)+1)):
                    a = self.position.y_up(size=i)
                    b = c.get_figure_on_position_color(self.color, a)
                    if b is None and i == abs(self.position.y - to_position.y):
                        print("tah byl uspesny")
                        self.position.x = to_position.x
                        self.position.y = to_position.y
                        break
                    elif b is None:
                        pass



                    elif b[0] == "ally":
                          raise ValueError("nejde stoji zde " + b[1] + " figura")

                    elif b[0] == "enemy":
                        if i == abs(self.position.y - to_position.y):
                                c.del_figure(b[2])
                                print("tah byl uspesny")
                                self.position.x = to_position.x
                                self.position.y = to_position.y
                        else:
                            raise ValueError("nelze")


            elif self.position.y - to_position.y > 0:
                for i in range(1,(abs(self.position.y - to_position.y)+1)):
                    a = self.position.y_up(size=(-i))
                    b = c.get_figure_on_position_color(self.color, a)
                    if b is None and i == abs(self.position.y - to_position.y):
                        print("tah byl uspesny")
                        self.position.x = to_position.x
                        self.position.y = to_position.y
                        break
                    elif b is None:
                        pass

                    elif b[0] == "ally":
                          raise ValueError("nejde stoji zde " + b[1] + " figura")

                    elif b[0] == "enemy":
                        if i == abs(self.position.y - to_position.y):
                                c.del_figure(b[2])
                                print("tah byl uspesny vzali jste figuru")
                                self.position.x = to_position.x
                                self.position.y = to_position.y
                        else:
                            raise ValueError("nelze")

        else:
            raise ValueError("position of y or x needs to be the same")


class Knight(Figure):
    def move(self, to_position):
        if ((ord(self.position.x) == ord(to_position.x) + 2 or (ord(self.position.x) == ord(to_position.x))) - 2) and ((self.position.y == to_position.y + 2) or (self.position.y == to_position.y - 2 )):

            a = Position(to_position.x, to_position.y)
            b = c.get_figure_on_position_color(self.color, a)


            if b == None:
                self.position.x = to_position.x
                self.position.y = to_position.y
                print("tah byl uspesny")



            elif b[0] == "ally":
                raise ValueError("nejde stoji zde " + b[1] + " figura")

            elif b[0] == "enemy":
                self.position.x = to_position.x
                self.position.y = to_position.y
                c.del_figure(b[2])
                print("tah byl uspesny vzali jste figuru")


        else:
            raise ValueError("position needs to be y +- 2 or ord(x) +- 2")



class Bishop(Figure):
    def move(self, to_position):
        rozpeti = (abs(to_position.y - self.position.y)+1)
        if ((self.position.x != to_position.x) and (self.position.y != to_position.y)):
            if (ord(to_position.x) - ord(self.position.x)) == (to_position.y - self.position.y):


                if ord(to_position.x) - ord(self.position.x) > 0:
                    for i in range (1,rozpeti):
                        a = self.position.x_y_change(i,i)
                        b = c.get_figure_on_position_color(self.color, a)
                        if b is None and i == rozpeti - 1:
                            print("tah byl uspesny")
                            self.position.x = to_position.x
                            self.position.y = to_position.y
                            break
                        elif b is None:
                            pass


                        elif b[0] == "ally":
                              raise ValueError("nejde stoji zde " + b[1] + " figura")

                        elif b[0] == "enemy":
                            if i == rozpeti - 1:
                                    c.del_figure(b[2])
                                    print("tah byl uspesny vzali jste figuru")
                                    self.position.x = to_position.x
                                    self.position.y = to_position.y
                            else:
                                raise ValueError("nelze")
                if ord(to_position.x) - ord(self.position.x) < 0:
                    for i in range (1,rozpeti):
                        a = self.position.x_y_change(-i,-i)
                        b = c.get_figure_on_position_color(self.color, a)
                        if b is None and i == rozpeti - 1:
                            print("tah byl uspesny")
                            self.position.x = to_position.x
                            self.position.y = to_position.y
                            break
                        elif b is None:
                            pass


                        elif b[0] == "ally":
                              raise ValueError("nejde stoji zde " + b[1] + " figura")

                        elif b[0] == "enemy":
                            if i == rozpeti - 1:
                                    c.del_figure(b[2])
                                    print("tah byl uspesny vzali jste figuru")
                                    self.position.x = to_position.x
                                    self.position.y = to_position.y
                            else:
                                raise ValueError("nelze")


            elif (ord(to_position.x) - ord(self.position.x)) == (-1)*(to_position.y - self.position.y):
                if ord(to_position.x) - ord(self.position.x) > 0:
                    for i in range (1,rozpeti):
                        a = self.position.x_y_change(i,-i)
                        b = c.get_figure_on_position_color(self.color, a)
                        if b is None and i == rozpeti - 1:
                            print("tah byl uspesny")
                            self.position.x = to_position.x
                            self.position.y = to_position.y
                            break
                        elif b is None:
                            pass


                        elif b[0] == "ally":
                              raise ValueError("nejde stoji zde " + b[1] + " figura")

                        elif b[0] == "enemy":
                            if i == rozpeti - 1:
                                    c.del_figure(b[2])
                                    print("tah byl uspesny vzali jste figuru")
                                    self.position.x = to_position.x
                                    self.position.y = to_position.y
                            else:
                                raise ValueError("nelze")
                elif ord(to_position.x) - ord(self.position.x) < 0:
                    for i in range (1,rozpeti):
                        a = self.position.x_y_change(-i,i)
                        b = c.get_figure_on_position_color(self.color, a)
                        if b is None and i == rozpeti - 1:
                            print("tah byl uspesny")
                            self.position.x = to_position.x
                            self.position.y = to_position.y
                            break
                        elif b is None:
                            pass


                        elif b[0] == "ally":
                              raise ValueError("nejde stoji zde " + b[1] + " figura")

                        elif b[0] == "enemy":
                            if i == rozpeti - 1:
                                    c.del_figure(b[2])
                                    print("tah byl uspesny vzali jste figuru")
                                    self.position.x = to_position.x
                                    self.position.y = to_position.y
                            else:
                                raise ValueError("nelze")

            else:
                raise ValueError("nelze")
        else:
            raise ValueError("nelze")



class Queen(Figure):
    def move(self, to_position):
        if ((self.position.x != to_position.x) or (self.position.y != to_position.y)):
            if (((self.position.x == to_position.x) and (self.position.y != to_position.y)) or (self.position.x != to_position.x) and (self.position.y == to_position.y)) or ((ord(to_position.x) - ord(self.position.x)) == (self.position.y - to_position.y)) or (((ord(to_position.x) - ord(self.position.x)) == (-1)*(self.position.y - to_position.y))):
                print("tah byl uspesny")
                self.position.x = to_position.x
                self.position.y = to_position.y
            else:
                raise ValueError("nelze")

        else:
            raise ValueError("nelze-")






c = Chessboard()


c.move_with_figure(Position('c', 2)).move(Position('c',3))
